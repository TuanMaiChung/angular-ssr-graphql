import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { GraphQLModule } from "./graphql.module";
import { HttpClientModule } from "@angular/common/http";
import { ExchangeRatesComponent } from "./exchange-rates/exchange-rates.component";
import { HomeComponent } from "./home/home.component";
import { ProductListComponent } from "./product-list/product-list.component";
import { ProductListService } from "./product-list/product-list.service";

@NgModule({
  declarations: [
    AppComponent,
    ExchangeRatesComponent,
    HomeComponent,
    ProductListComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    AppRoutingModule,
    GraphQLModule,
    HttpClientModule,
  ],
  providers: [ProductListService],
  bootstrap: [AppComponent],
})
export class AppModule {}
