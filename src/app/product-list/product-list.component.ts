import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { ProductItem, ProductListService } from "./product-list.service";

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.scss"],
})
export class ProductListComponent implements OnInit {
  productList: Observable<ProductItem[]>;

  constructor(private productListService: ProductListService) {}

  ngOnInit() {
    this.productList = this.productListService.getProductList();
  }
}
