import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';

export interface ProductItem {
  _id: string;
  name: string;
  image: string;
  thumbnail: string;
  shortDescription: string;
  categoryId: string;
  salePrice: number;
  originalPrice: number;
}

@Injectable({ providedIn: "root" })
export class ProductListService {
  constructor(private httpClient: HttpClient) {}

  getProductList(): Observable<ProductItem[]> {
    return this.httpClient
      .get("https://powerful-sea-64217.herokuapp.com/api/products?page=3")
      .pipe(
        map((response: any) => {
          return response.body;
        })
      );
  }
}
